package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Rectangle;

	/**
	 * ...
	 * @author takumi
	 */
	public class Box extends Sprite
	{
		private var bd:BitmapData;
		public var border:int;
		public var borderColor:uint;
		public var faceColor:uint;
		
		public function Box(width:int, height:int, border:int, borderColor:uint = 0xFF000000, faceColor:uint = 0xFFFFFFFF)
		{
			this.border = border;
			this.borderColor = borderColor;
			this.faceColor = faceColor;
			bd = new BitmapData(width, height, false);
			repaint();
			addChild(new Bitmap(bd));
		}
		
		private function repaint():void
		{
			bd.fillRect(new Rectangle(0, 0, bd.width, bd.height), borderColor);
			bd.fillRect(new Rectangle(border, border, bd.width - (border * 2), bd.height - (border * 2)), faceColor);
		}
	}

}