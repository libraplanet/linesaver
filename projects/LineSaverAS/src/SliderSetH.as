package  
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	/**
	 * ...
	 * @author takumi
	 */
	public class SliderSetH extends Sprite
	{
		public var caption:TextField = new TextField();
		public var slider:HSlider;
		public function SliderSetH(fontsize:int, width:int) 
		{
			slider = new HSlider(width);
			{
				var textFormat:TextFormat = new TextFormat();
				textFormat.size = fontsize;
				caption.autoSize = TextFieldAutoSize.LEFT;
				caption.selectable = false;
				caption.defaultTextFormat = textFormat;
				caption.x = 0;
				caption.y = 0;
				caption.textColor = 0xFFFFFF;
			}
			addChild(caption);
			addChild(slider);
		}
	}

}