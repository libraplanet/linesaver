package  
{
	import flash.display.BitmapData;
	import flash.display.Shape;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import org.flashdevelop.utils.FlashConnect;
	
	/**
	 * ...
	 * @author takumi
	 */
	public class Viewer 
	{
		private var pointer:Vector.<Pointer> = new Vector.<Pointer>();
		private var list:Vector.<Vector.<Point>> = new Vector.<Vector.<Point>>();
		private var histry:int = 10;
		private var movement:Number = 5.0;
	
		public function Viewer() 
		{
			var i:int;
			
			for (i = 0; i < 6; i++)
			{
				pointer.push(new Pointer());
				pointer[i].init();
			}
		}
		
		public function getMovementNum():Number
		{
			return movement;
		}
		
		public function setMovementNum(movement:Number):void
		{
			this.movement = movement;
			
		}
		
		public function getHistryNum():int
		{
			return histry;
		}
		
		public function setHistryNum(histry:int):void
		{
			this.histry = histry;
			while (list.length > histry)
			{
				list.shift();
			}
		}
		
		public function getPointerNum():int
		{
			return pointer.length;
		}
		
		public function setPointerNum(n:int):void
		{
			for (var i:int = pointer.length; i < n; i++)
			{
				pointer.push(new Pointer());
				pointer[i].init();
			}
			while (pointer.length > n)
			{
				pointer.pop();
			}
		}
		
		public function update():void
		{
			var v:Vector.<Point> = new Vector.<Point>()
			for (var i:int = 0; i < pointer.length; i++)
			{
				pointer[i].update(movement);
				v.push(new Point(pointer[i].getPos().x, pointer[i].getPos().y));
			}
			
			list.push(v);
			if (list.length > histry)
			{
				list.shift();
			}
		}
		
		private static function getLinearColor(c0:uint, c1:uint, v:Number):uint
		{
			var a0:int = (c0 >> 24) & 0xFF;
			var r0:int = (c0 >> 16) & 0xFF;
			var g0:int = (c0 >> 8) & 0xFF;
			var b0:int = (c0 >> 0) & 0xFF;
			var a1:int = (c1 >> 24) & 0xFF;
			var r1:int = (c1 >> 16) & 0xFF;
			var g1:int = (c1 >> 8) & 0xFF;
			var b1:int = (c1 >> 0) & 0xFF;
			var a:int = int(a0 + int((a1 - a0) * v));
			var r:int = int(r0 + int((r1 - r0) * v));
			var g:int = int(g0 + int((g1 - g0) * v));
			var b:int = int(b0 + int((b1 - b0) * v));
			return uint((a << 24) | (r << 16) | (g << 8) | (b << 0));
		}
		
		public function draw(bd:BitmapData):void
		{
			var bgcolor:uint = 0xFF000033;
			bd.fillRect(new Rectangle(0, 0, bd.width, bd.height), bgcolor);
			
			//pointer
			{
				for (var p:int = 0; p < histry; p++)
				{
					//var q:int = ((index - p - 1) + list.length) % list.length;
					var q:int =  p - (histry - list.length);
					if (q >= 0)
					{
						var v:Vector.<Point> = list[q];
						var c:uint = getLinearColor(bgcolor, 0xFFFF0000, (p + 1) / histry);

						var shape:Shape = new Shape();
						shape.graphics.lineStyle(1, c);
						shape.graphics.moveTo(v[0].x, v[0].y);
						for (var i:int = 0; i < v.length; i++)
						{
							var j:int = (i + 1) % v.length;
							shape.graphics.lineTo(v[j].x, v[j].y);
						}
						bd.draw(shape);
					}
				}
			}
		}
	}

}