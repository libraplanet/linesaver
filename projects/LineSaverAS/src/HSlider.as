package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.GraphicsSolidFill;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author takumi
	 */
	public class HSlider extends Sprite
	{
		private var bar:Box;
		private var box:Box;
		private var min:int;
		private var max:int;
		private var value:int;
		public var valueChenged:Function = null;
		
		public function HSlider(width:int)
		{
			bar = new Box(width, 20, 2, 0xFF666666, 0xFFFFFF);
			box = new Box(20, 40, 2, 0xFF666666, 0xFF5588);
			
			bar.x = 0;
			bar.y = 10;
			
			box.x = 0;
			box.y = 0;
			
			addChild(bar);
			addChild(box);

			bar.addEventListener(MouseEvent.MOUSE_DOWN, onBarClickHandler);
			box.addEventListener(MouseEvent.MOUSE_UP, onBoxDropHandler);
			box.addEventListener(MouseEvent.MOUSE_DOWN, onBoxDragHandler);
		}
		
		public function setMin(n:int):void
		{
			this.min = n;
		}
		public function setMax(n:int):void
		{
			this.max = n;
		}
		
		public function setValue(n:int):void
		{
			//filter
			{
				n = Math.max(Math.min(n, max), min);
			}
			//set
			{
				this.value = n;
				box.x = ((n - min) * (bar.width - box.width) / (max - min));
				if (valueChenged != null)
				{
					valueChenged();
				}
			}
		}
		
		public function getValue():int
		{
			return this.value;
		}
		
		
		private function boxin(x:int):int
		{
			if (x < 0)
			{
				x = 0;
			}
			if (x + box.width > bar.width)
			{
				x = bar.width - box.width;
			}
			return x;
		}
		
		private function PosXToValue(x:int):int
		{
			return (x * (max - min) / (bar.width - box.width));
		}
		
		private function onBoxDragHandler(event:MouseEvent):void
		{
			this.addEventListener(MouseEvent.MOUSE_MOVE, onBoxMoveHandler);
		}

		private function onBarClickHandler(event:MouseEvent):void
		{
			setValue(min + PosXToValue(boxin(event.localX - (box.width / 2))));
		}

		private function onBoxMoveHandler(event:MouseEvent):void
		{
			setValue(min + PosXToValue(boxin(mouseX - (box.width / 2))));
		}

		private function onBoxDropHandler(event:MouseEvent):void
		{
			this.removeEventListener(MouseEvent.MOUSE_MOVE, onBoxMoveHandler);
		}
	}
}