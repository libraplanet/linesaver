package  
{
	import adobe.utils.CustomActions;
	import flash.geom.Point;
	import org.flashdevelop.utils.FlashConnect;

	/**
	 * ...
	 * @author takumi
	 */
	public class Pointer 
	{
		
		private var deg:Number;
		private var pos:Point = new Point();
		
		public function Pointer() 
		{
			
		}
		
		public function getPos():Point
		{
			return pos;
		}
		
		public function init():void
		{
			pos.x = Math.random() * 800;
			pos.y = Math.random() * 600;
			deg = Math.random() * 360;
		}
		
		private static function deg2rad(deg:Number):Number
		{
			return deg * Math.PI / 180;
		}
		
		private static function rad2deg(rad:Number):Number
		{
			return rad * 180 / Math.PI;
		}
		
		private static function degFlipX(deg:Number):Number
		{
			var ret:Number = rad2deg(Math.atan2(Math.sin(deg2rad(deg)), -Math.cos(deg2rad(deg))));
			ret += 360;
			ret %= 360;
			return ret;
		}
		
		private static function degFlipY(deg:Number):Number
		{
			var ret:Number = degFlipX((deg + 90) % 360);

			ret -= 90;
			ret += 360;
			ret %= 360;
			return ret;
		}
		
		public function update(v:Number):void
		{
			pos.x += Math.cos(deg2rad(deg)) * v;
			pos.y += Math.sin(deg2rad(deg)) * v;
			
			{
				if (pos.x < 0)
				{
					pos.x = 0 + (pos.x - 0);
					deg = degFlipX(deg);
				}
				if (pos.x > 800)
				{
					pos.x = 800 - (pos.x - 800);
					deg = degFlipX(deg);
				}
				if (pos.y < 0)
				{
					pos.y = 0 + (pos.y - 0);
					deg = degFlipY(deg);
				}
				if (pos.y > 600)
				{
					pos.y = 600 - (pos.y - 600);
					deg = degFlipY(deg);
				}
			}
		}
	}
}