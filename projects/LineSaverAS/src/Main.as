package 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author takumi
	 */
	public class Main extends Sprite 
	{
		private var viewer:Viewer;
		private var bd:BitmapData;
		
		private var hsMovNum:SliderSetH;
		private var hsHistNum:SliderSetH;
		private var hsPointNum:SliderSetH;

		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			{
				bd = new BitmapData(800, 600, true);
				viewer = new Viewer();

				//move
				{
					hsMovNum = new SliderSetH(24, 250);
					hsMovNum.x = 30;
					hsMovNum.y = 450;
					hsMovNum.slider.x = 0;
					hsMovNum.slider.y = 28;
					hsMovNum.slider.setMin(0);
					hsMovNum.slider.setMax(100);
					hsMovNum.slider.valueChenged = function():void
					{
						viewer.setMovementNum(hsMovNum.slider.getValue() / 10);
						hsMovNum.caption.text = "移動量 : " + (hsMovNum.slider.getValue() / 10);
					}
					hsMovNum.slider.setValue(viewer.getMovementNum() * 10);
				}
				
				//hist
				{
					hsHistNum = new SliderSetH(24, 300);
					hsHistNum.x = 430;
					hsHistNum.y = 450;
					hsHistNum.slider.x = 0;
					hsHistNum.slider.y = 28;
					hsHistNum.slider.setMin(1);
					hsHistNum.slider.setMax(100);
					hsHistNum.slider.valueChenged = function():void
					{
						viewer.setHistryNum(hsHistNum.slider.getValue());
						hsHistNum.caption.text = "残像数 : " + hsHistNum.slider.getValue();
					}
					hsHistNum.slider.setValue(viewer.getHistryNum());
				}

				//point
				{
					hsPointNum = new SliderSetH(24, 350);
					hsPointNum.x = 30;
					hsPointNum.y = 520;
					hsPointNum.slider.x = 0;
					hsPointNum.slider.y = 28;
					hsPointNum.slider.setMin(3);
					hsPointNum.slider.setMax(100);
					hsPointNum.slider.valueChenged = function():void
					{
						viewer.setPointerNum(hsPointNum.slider.getValue())
						hsPointNum.caption.text = "ポイント数 : " + hsPointNum.slider.getValue();
					}
					hsPointNum.slider.setValue(viewer.getPointerNum());
				}
			}
			
			//add
			{
				this.addChild(new Bitmap(bd));
				this.addChild(hsMovNum);
				this.addChild(hsHistNum);
				this.addChild(hsPointNum);
				this.addEventListener(Event.ENTER_FRAME, tick);
			}
		}

		/**
		 * 
		 * @param	event
		 */
		private function tick(event:Event):void
		{
			viewer.update();
			viewer.draw(bd);
		}
	}
}