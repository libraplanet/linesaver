using System;
using System.Drawing;

public class Pointer 
{
	
	private float deg;
	private PointF pos = new Point();
	private static Random rand = new System.Random();
	
	public Pointer()
	{
		
	}
	
	public PointF GetPos()
	{
		return pos;
	}
	
	public void Init()
	{
		pos.X = (float)(rand.NextDouble() * 800);
		pos.Y = (float)(rand.NextDouble() * 600);
		deg = (float)(rand.NextDouble() * 360);
	}
	
	private static double Deg2rad(double deg)
	{
		return deg * Math.PI / 180;
	}
	
	private static double Rad2deg(double rad)
	{
		return rad * 180 / Math.PI;
	}
	
	private static double DegFlipX(double deg)
	{
		double ret = Rad2deg(Math.Atan2(Math.Sin(Deg2rad(deg)), -Math.Cos(Deg2rad(deg))));
		ret += 360;
		ret %= 360;
		return ret;
	}
	
	private static double degFlipY(double deg)
	{
		double ret = DegFlipX((deg + 90) % 360);

		ret -= 90;
		ret += 360;
		ret %= 360;
		return ret;
	}
	
	public void Update(float v)
	{
		pos.X += (float)Math.Cos(Deg2rad(deg)) * v;
		pos.Y += (float)Math.Sin(Deg2rad(deg)) * v;
		
		{
			if (pos.X < 0)
			{
				pos.X = 0 + (pos.X - 0);
				deg = (float)DegFlipX(deg);
			}
			if (pos.X > 800)
			{
				pos.X = 800 - (pos.X - 800);
				deg = (float)DegFlipX(deg);
			}
			if (pos.Y < 0)
			{
				pos.Y = 0 + (pos.Y - 0);
				deg = (float)degFlipY(deg);
			}
			if (pos.Y > 600)
			{
				pos.Y = 600 - (pos.Y - 600);
				deg = (float)degFlipY(deg);
			}
		}
	}
}
