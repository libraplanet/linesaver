using System;
using System.Windows;
using System.Windows.Forms;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;

class Viewer
{
	Form _form;
	Form Form {get {return _form;}}

	private List<Pointer> pointer = new List<Pointer>();
	private List<List<PointF>> list = new List<List<PointF>>();
	private int histry = 10;
	private float movement = 5.0f;


	public Viewer(Form f)
	{
		this._form = f;

		for (int i = 0; i < 6; i++)
		{
			pointer.Add(new Pointer());
			pointer[i].Init();
		}
	}

	public float getMovementNum()
	{
		return movement;
	}
	
	public void setMovementNum(float movement)
	{
		this.movement = movement;
		
	}

	public int getHistryNum()
	{
		return histry;
	}
	
	public void setHistryNum(int histry)
	{
		this.histry = histry;
		while (list.Count > histry)
		{
			list.RemoveAt(0);
		}
	}
	
	public int getPointerNum()
	{
		return pointer.Count;
	}

	public void setPointerNum(int n)
	{
		for (int i = pointer.Count; i < n; i++)
		{
			pointer.Add(new Pointer());
			pointer[i].Init();
		}
		while (pointer.Count > n)
		{
			pointer.RemoveAt(pointer.Count - 1);
		}
	}
	
	public void Init()
	{
	}

	public void Update()
	{
		List<PointF> v = new List<PointF>();
		for (int i = 0; i < pointer.Count; i++)
		{
			pointer[i].Update(movement);
			v.Add(new PointF(pointer[i].GetPos().X, pointer[i].GetPos().Y));
		}

		list.Add(v);
		if (list.Count > histry)
		{
			list.RemoveAt(0);
		}
	}

	private static Color getLinearColor(Color c0, Color c1, float v)
	{
		int a = (int)(c0.A + (int)((c1.A - c0.A) * v));
		int r = (int)(c0.R + (int)((c1.R - c0.R) * v));
		int g = (int)(c0.G + (int)((c1.G - c0.G) * v));
		int b = (int)(c0.B + (int)((c1.B - c0.B) * v));
		return Color.FromArgb(a, r, g, b);
	}


	public void Draw(Graphics g)
	{

		Color bgcolor = Color.FromArgb(255, 0, 0, 0x33);
		g.FillRectangle(new SolidBrush(bgcolor), 0, 0, Form.ClientSize.Width, Form.ClientSize.Height);

		//pointer
		{
			for (int p = 0; p < histry; p++)
			{
				//var q:int = ((index - p - 1) + list.length) % list.length;
				int q =  p - (histry - list.Count);
				if (q >= 0)
				{
					List<PointF> v = list[q];
					Color c = getLinearColor(bgcolor, Color.FromArgb(255, 255, 0, 0), (float)(p + 1) / histry);
					Pen pen = new Pen(c, 1);
					g.DrawPolygon(pen, v.ToArray());
					/*
					for(int i = 0; i < v.Count; i++)
					{
						int j = (i + 1) % v.Count;
						g.DrawLine(pen, v[i].X, v[i].Y, v[j].X, v[j].Y);
						//Trace.WriteLine("[Viewer#Draw()] q(" + q + ") c(" + c + ") v[" + i + "](" + v[i].X + ", " + v[i].Y + ")");
					}
					*/

					/*
					var shape:Shape = new Shape();
					shape.graphics.lineStyle(1, c);
					shape.graphics.moveTo(v[0].x, v[0].y);
					for (var i:int = 0; i < v.length; i++)
					{
						var j:int = (i + 1) % v.length;
						shape.graphics.lineTo(v[j].x, v[j].y);
					}
					bd.draw(shape);
					*/
				}
			}
		}
	}
}
