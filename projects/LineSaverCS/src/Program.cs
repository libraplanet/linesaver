using System;
using System.Windows;
using System.Windows.Forms;
using System.Drawing;

class Program
{
	static void Main()
	{
		using(Form f = new MyForm())
		{
			Application.Run(f);
		}
	}

	class MyForm : Form
	{
		private Viewer viewer;
		private SliderSetH hsMovNum;
		private SliderSetH hsHistNum;
		private SliderSetH hsPointNum;

		public MyForm()
		{
			this.SuspendLayout();
			//instance
			{
				viewer = new Viewer(this);

				//timer
				{
					Timer t = new Timer();
					t.Interval = (int)(1000 / 30);
					t.Enabled = true;
					t.Tick += new EventHandler(this.Tick);
				}
			}
			//design
			{
				this.FormBorderStyle = FormBorderStyle.FixedSingle;
				this.ClientSize = new Size(800, 600);
				this.DoubleBuffered = true;
				this.MaximizeBox = false;
				this.Text = "LineSaverCS";

				{
					//move
					{
						hsMovNum = new SliderSetH(16, 250);
						hsMovNum.Location = new Point(30, 450);
						hsMovNum.slider.Location = new Point(0, 28);
						hsMovNum.slider.Minimum = 0;
						hsMovNum.slider.Maximum = 100;
						hsMovNum.slider.ValueChanged += delegate (object sender, EventArgs e)
						{
							viewer.setMovementNum(hsMovNum.slider.Value / 10.0f);
							hsMovNum.caption.Text = "移動量 : " + (hsMovNum.slider.Value / 10.0f);
						};
						hsMovNum.slider.Value = (int)(viewer.getMovementNum() * 10);
					}

					//hist
					{
						hsHistNum = new SliderSetH(16, 300);
						hsHistNum.Location = new Point(470, 450);
						hsHistNum.slider.Location = new Point(0, 28);
						hsHistNum.slider.Minimum = 1;
						hsHistNum.slider.Maximum = 100;
						hsHistNum.slider.ValueChanged += delegate (object sender, EventArgs e)
						{
							viewer.setHistryNum(hsHistNum.slider.Value);
							hsHistNum.caption.Text = "残像数 : " + hsHistNum.slider.Value;
						};
						hsHistNum.slider.Value = viewer.getHistryNum();
					}

					//point
					{
						hsPointNum = new SliderSetH(16, 350);
						hsPointNum.Location = new Point(30, 520);
						hsPointNum.slider.Location = new Point(0, 28);
						hsPointNum.slider.Minimum = 3;
						hsPointNum.slider.Maximum = 100;
						hsPointNum.slider.ValueChanged += delegate (object sender, EventArgs e)
						{
							viewer.setPointerNum(hsPointNum.slider.Value);
							hsPointNum.caption.Text = "ポイント数 : " + hsPointNum.slider.Value;
						};
						hsPointNum.slider.Value = viewer.getPointerNum();
					}
				}
				Controls.Add(hsMovNum);
				Controls.Add(hsHistNum);
				Controls.Add(hsPointNum);
				
				hsPointNum.BringToFront();
			}
			//load
			{
				viewer.Init();
			}
			this.ResumeLayout(false);
		}

		void Tick(object sender, EventArgs e)
		{
			viewer.Update();
			this.Invalidate(true);
			this.Update();
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			viewer.Draw(e.Graphics);
		}
	}
}