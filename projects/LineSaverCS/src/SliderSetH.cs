using System;
using System.Windows.Forms;
using System.Drawing;

public class SliderSetH : UserControl
{
	public Label caption = new Label();
	public TrackBar slider;

	public SliderSetH(int fontsize, int width)
	{
		this.AutoSize = true;
		slider = new TrackBar();
		{
			caption.AutoSize = true;
			caption.ForeColor = Color.FromArgb(255, 255, 255, 255);
			caption.Font = new Font(caption.Font.Name, fontsize);
			slider.Size = new Size(width, slider.Size.Height);
		}
		this.BackColor = Color.Transparent;
		this.DoubleBuffered = true;
		Controls.Add(caption);
		Controls.Add(slider);
	}
}
